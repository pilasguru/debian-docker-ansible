
A Vagrantfile to configure a vanilla Debian 8 "Jessie" Linux box as a DOCKER server

Also download, install and run a first image "Hello World"..

It uses *shell* provisioning with a hope of future improvement to ansible-local provisioning.


## Installation

```
git clone https://gitlab.com/pilasguru/debian-docker-ansible.git
cd debian-docker-ansible
vagrant up
```


## License

```
The MIT License (MIT)
 
Copyright (c) 2016 Rodolfo Pilas <rodolfo@pilas.guru>
```




